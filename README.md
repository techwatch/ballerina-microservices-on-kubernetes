# Ballerina Programming Language - V 0.991

[Ballerina](https://v1-0.ballerina.io/) is an open source programming language and platform for cloud-era application programmers to easily write software that just works.

## Prerequisites

* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) or [OpenJDK 8](http://openjdk.java.net/install/)
* [Node (v8.9.x) + npm (v5.6.0 or later)](https://nodejs.org/en/download/)
* [Docker](https://www.docker.com/get-docker)
* [Kubernetes](https://kubernetes.io/)
* [Ballerina](https://ballerina.io/learn/getting-started/)

* [VSC](https://v1-0.ballerina.io/learn/tools-ides/vscode-plugin/) or [IntelliJ](https://v1-0.ballerina.io/learn/tools-ides/intellij-plugin/) Ballerina plugin


## See more

* [Examples](https://ballerina.io/learn/by-example/) 
* [Ballerina central](https://central.ballerina.io/)
* [Publishing config](https://central.ballerina.io/publish-module)

## Todo
![Todo](img/sequencediagram.png)
### Validation tests (TDD)
When deploy the services on the cluster, verify that the following calls work:
 - should succeed only inside the cluster
```bash
curl GET http://wiarriors-service:1989/api/v1/warriors
```
- Outside the cluster with a valid apikey should succeed
```bash
curl GET http://techwatch.io/api/v1/warriors -H "apikey: Z2FtZTpnYW1lMQ==" 
```
- Outside the cluster with a invalid apikey should fail
```bash
curl GET http://techwatch.io/api/v1/warriors -H "apikey: G02TheBeach==" 
``` 
### [Step 0] Init project
In IntelliJ : `FIle` -> `New` -> `Project` then:
![Todo](img/new-project.png)

Alternatively :
```bash
# use ballerina init command inside the work directory   
ballerina init
``` 
